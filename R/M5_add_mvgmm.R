#' 增加mvgmm方法
#'
#' @param mv_res_all M5_mvmr生成的结果
#' @param object MV_Input
#' @param nx 暴露的样本量,例如c(17723,17724,17725)
#' @param ny 结局的样本量,例如17726
#'
#' @return 增加mvgmm方法后的多变量的结果
#' @export
#'
#' @examples
#'
#' \dontrun{
#'
#' library(Oneclick)
#'
#' mv_res_all <- M5_mvmr(MV_Input)
#'
#' mv_res_all <- mv_res_all %>% M5_add_mvgmm(object=MV_Input,
#'                                          nx= c(17723,17724,17725),
#'                                          ny= 17726 )
#'
#' }
#'
#'
#'
#'
#'
#'
M5_add_mvgmm<-function(mv_res_all=mv_res_all,
                       object=MV_Input,
                       nx,
                       ny){

  mv_res_temp <- MendelianRandomization::mr_mvgmm(MV_Input,nx=nx,ny=ny)
  mv_res_temp <- data.frame( exposure = mv_res_temp@Exposure,
                             outcome = mv_res_temp@Outcome,
                             nsnp = nrow(mv_res_temp@Correlation),
                             method = "mr_mvgmm"  ,
                             b = mv_res_temp@Estimate,
                             se = mv_res_temp@StdError,
                             pval = mv_res_temp@Pvalue) %>%
    TwoSampleMR::generate_odds_ratios()

  mv_res_temp$`Beta (95% CI)` <- ifelse(is.na(mv_res_temp$b), NA,
                                        sprintf("%.3f (%.3f to %.3f)",
                                                mv_res_temp$b, mv_res_temp$lo_ci, mv_res_temp$up_ci))


  mv_res_temp$`OR (95% CI)` <- ifelse(is.na(mv_res_temp$or), NA,
                                      sprintf("%.3f (%.3f to %.3f)",
                                              mv_res_temp$or, mv_res_temp$or_lci95, mv_res_temp$or_uci95))


  mv_res_all <- dplyr::bind_rows(mv_res_all,mv_res_temp )
  return(mv_res_all)
}
