#' 多暴露多结局森林图
#'
#' @param res U5_mr分析出的res
#' @param outcome_order 可以对结局进行排序，默认为unique(res$outcome)
#' @param pal_color 调色板，默认为frontiers配色
#'
#' @return 森林图拼图
#' @export
#'
#' @examples
#'
#' \dontrun{
#'
#' p_all<-U7_forest_plot(res)
#'
#'
#' ggplot2::ggsave("Figure 1.png",
#'                  plot= p_all,
#'                  width=50, height=21,units=c("cm"),
#'                  dpi=900, limitsize=F )
#'
#' # 只作阳性的结果的图
#' ivw_sig<-subset(res,res$`pval_Inverse variance weighted`< 0.05)
#' P1<-U7_forest_plot(ivw_sig)
#' P1
#'
#' }
#'
#'
U7_forest_plot<-function( res,outcome_order = "",   pal_color= ggsci::pal_frontiers("default")(10) ){

  require(ggplot2)

  df<-res[,c("exposure_Inverse variance weighted","outcome_Inverse variance weighted","or_Inverse variance weighted",
             "or_lci95_Inverse variance weighted","or_uci95_Inverse variance weighted","pval_Inverse variance weighted")]

  colnames(df)<-c("exposure","outcome","or",
                  "or_lci95","or_uci95","pval")

  plot_outcome <- function(df, outcome,single_color ){
    # 选取当前Outcome的数据
    df_subset = df[df$outcome == outcome, ]

    # 绘制图形
    p <- ggplot(df_subset, aes(x = or, y = exposure)) +
      geom_vline(xintercept = 1, linetype = 2) +
      geom_errorbarh(aes(xmin = or_lci95, xmax = or_uci95), height = 0.2,color = single_color) +
      geom_point(size = 3,color =single_color) +
      labs(x = "Odds Ratio", y = "exposure", title = outcome) +
      theme_minimal()

    return(p)
  }

  if(outcome_order == "" ){
    outcome_order<-unique(df$outcome)
  }

  plots_list<-list()

  for (i in 1:length(outcome_order) ) {
    plots_list[[i]]<- plot_outcome(df,outcome_order[i],pal_color[i])
  }

  p_all<- do.call(gridExtra::grid.arrange,plots_list )
  return(p_all)
}
