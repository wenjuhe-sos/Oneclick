#' 计算条件F值
#'
#' @param MV_Input M4_mv_dat_to_MV_Input方法生成的MV_Input
#'
#' @return 多变量的条件F值，与单变量的F值有差别
#' @export
#'
#' @examples
#'
#' \dontrun{
#' library(Oneclick)
#' strength_mvmr <- M7_strength_mvmr(MV_Input)
#' }
#'
#'
M7_strength_mvmr <- function(MV_Input){

  ins <- rownames( installed.packages() )
  if(!"MVMR" %in% ins ){
    remotes::install_github("WSpiller/MVMR", build_opts = c("--no-resave-data", "--no-manual"), build_vignettes = TRUE)
  }

  require(MVMR)

  mvmr_dat <- format_mvmr(
    BXGs = MV_Input@betaX ,
    BYG = MV_Input@betaY,
    seBXGs = MV_Input@betaXse ,
    seBYG = MV_Input@betaYse,
    RSID = MV_Input@snps)


  strength_mvmr <- strength_mvmr(mvmr_dat, gencov=0)

  colnames(strength_mvmr) <- colnames(MV_Input@betaX)

  strength_mvmr<-t(strength_mvmr)

  colnames(strength_mvmr) <- "Conditional F-statistics"

  message("条件F值大于10说明多变量的分析受到弱工具变量的影响，引用https://pubmed.ncbi.nlm.nih.gov/34338327/。")


  return(strength_mvmr)

}
